plugins {
    application
    java
    id("com.github.ben-manes.versions") version "0.21.0"
}

application {
    mainClassName = "org.mpierce.guice.demo.GuiceDemo"
}

repositories {
    jcenter()
}

dependencies {
    implementation("com.google.inject:guice:4.2.2")
}

java {
    sourceCompatibility = JavaVersion.VERSION_11
    targetCompatibility = JavaVersion.VERSION_11
}
