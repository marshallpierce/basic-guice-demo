package org.mpierce.guice.demo;

import com.google.inject.Inject;

/**
 * Imagine that this logic is being wired up to handle http requests via your favorite http server library.
 */
public class HttpEndpoint {

    private final SecretBusinessLogic secretBusinessLogic;

    @Inject
    HttpEndpoint(SecretBusinessLogic secretBusinessLogic) {
        this.secretBusinessLogic = secretBusinessLogic;
    }

    String handlePostRequest() {
        return secretBusinessLogic.calculateProprietaryBusinessStuff();
    }

}
