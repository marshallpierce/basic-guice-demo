package org.mpierce.guice.demo;

import com.google.inject.Inject;
import java.util.List;

/**
 * This might be a client for some internal service, or Twitter's API, or whatever else.
 */
public class ApiClient {

    @Inject
    ApiClient(Config config) {
        System.out.println("Creating a client with endpoint " + config.getApiEndpoint());
    }

    public List<String> getSomeOtherData() {
        return List.of("foo", "bar", "baz");
    }
}
