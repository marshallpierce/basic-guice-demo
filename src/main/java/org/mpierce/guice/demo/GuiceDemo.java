package org.mpierce.guice.demo;

import com.google.inject.AbstractModule;
import com.google.inject.Guice;
import com.google.inject.Injector;
import com.google.inject.Provides;
import com.google.inject.Scopes;
import com.google.inject.Singleton;
import com.google.inject.Stage;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

public class GuiceDemo {
    public static void main(String[] args) throws ExecutionException, InterruptedException {
        // you might load your config data here, for instance, before you boot up the rest of the system via Guice.
        Config config = new Config();

        Injector injector = Guice.createInjector(Stage.PRODUCTION, new DemoModule(config));

        // get some instances
        HttpEndpoint endpoint = injector.getInstance(HttpEndpoint.class);
        BackgroundJob backgroundJob = injector.getInstance(BackgroundJob.class);

        // let's actually run the background job to show that it works
        ExecutorService pool = Executors.newCachedThreadPool();

        Future<String> future = pool.submit(backgroundJob);

        System.out.println("Got result from background job: " + future.get());

        pool.shutdown();
    }

    static class DemoModule extends AbstractModule {
        // this object is passed into the module to be bound
        private final Config config;

        DemoModule(Config config) {
            this.config = config;
        }

        @Override
        protected void configure() {
            // set sane (stringent) defaults for how much magic Guice will use -- i.e. as little as possible
            binder().requireExplicitBindings();
            binder().requireAtInjectOnConstructors();
            binder().requireExactBindingAnnotations();

            // Will call the ApiClient ctor only once -- creating http clients in practice is fairly expensive so you
            // would want to have only one..
            bind(ApiClient.class).in(Scopes.SINGLETON);

            // this class, however, is cheap to create, so we'll just let Guice invoke the ctor as many times as needed
            bind(SecretBusinessLogic.class);

            bind(HttpEndpoint.class);
            bind(BackgroundJob.class);

            bind(Config.class).toInstance(config);
        }

        /**
         * Binding via a Provides method. In this simple case, this could easily be done via bind() as well, but using a
         * method is great for constructing types you don't control (e.g. from some other library).
         *
         * Like ApiClient, we're pretending this allocates real resources, so we bind it as a singleton.
         */
        @Provides
        @Singleton
        Database getDatabase() {
            return new Database();
        }
    }
}
