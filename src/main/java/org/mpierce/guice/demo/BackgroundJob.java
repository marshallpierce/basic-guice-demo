package org.mpierce.guice.demo;

import com.google.inject.Inject;
import java.util.concurrent.Callable;

/**
 * Just another consumer of {@link SecretBusinessLogic}.
 */
public class BackgroundJob implements Callable<String> {

    private final SecretBusinessLogic secretBusinessLogic;

    @Inject
    BackgroundJob(SecretBusinessLogic secretBusinessLogic) {
        this.secretBusinessLogic = secretBusinessLogic;
    }

    @Override
    public String call() throws Exception {
        return secretBusinessLogic.calculateProprietaryBusinessStuff();
    }
}
