package org.mpierce.guice.demo;

import java.util.List;

/**
 * This could be a db connection, or a DAO of some kind, or ...
 *
 * This class is created in a Provides method, so it doesn't need an @Inject ctor.
 */
public class Database {

    /**
     * @return some critically important business data
     */
    public List<String> getData() {
        return List.of("one", "two", "three");
    }
}
