package org.mpierce.guice.demo;

import com.google.inject.Inject;
import java.util.ArrayList;
import java.util.List;
import javax.sound.midi.Soundbank;

/**
 * This class only exists to add a little more complexity to the object graph.
 */
public class SecretBusinessLogic {

    private final Database database;
    private final ApiClient apiClient;

    @Inject
    SecretBusinessLogic(Database database, ApiClient apiClient) {
        System.out.println("SecretBusinessLogic ctor called");
        this.database = database;
        this.apiClient = apiClient;
    }

    String calculateProprietaryBusinessStuff() {
        // step 1: do some business logic
        var list = new ArrayList<String>();
        list.addAll(database.getData());
        list.addAll(apiClient.getSomeOtherData());

        // step 3: profit
        return String.join(",", list);
    }
}
